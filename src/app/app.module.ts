import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RoutesDetailComponent, RoutesDetailResolver, RoutesDetailModule } from './routes-detail';
import { RoutesListComponent, RoutesListModule } from './routes-list';
import { RoutesService } from './routes.service';

const appRoutes: Routes = [
  {
    path: 'routes/:id',
    component: RoutesDetailComponent,
    resolve: {
      route: RoutesDetailResolver
    }
  },
  {
    path: 'routes',
    component: RoutesListComponent
  },
  { path: '',
    redirectTo: '/routes',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    RoutesDetailModule,
    RoutesListModule
  ],
  providers: [
    RoutesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
