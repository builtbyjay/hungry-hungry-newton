import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IRouteResponse, IRoute, ILocationResponse, LatLngAlt } from './route';

@Injectable()
export class RoutesService {
  private url = 'https://infinite-lake-80504.herokuapp.com/api/routes';

  constructor(private http: HttpClient) { }

  public all(): Observable<IRoute[]> {
    return this.http.get<IRoute[]>(this.url);
  }

  public find(id: number|string): Observable<IRoute> {
    return this.http.get<IRouteResponse>(`${this.url}/${id}`).pipe(
      map((route: IRouteResponse) => {
        return {
          id: route.id,
          name: route.name,
          coordinates: route.locations.map((location: ILocationResponse) => {
            return new LatLngAlt(
              location.latitude,
              location.longitude,
              location.altitude
            );
          })
        }
      })
    );
  }
}
