import { Injectable } from '@angular/core';

import { Stage, LatLngAlt } from '../route';

@Injectable()
export class RoutesHelperService {

  public createStages(coordinates: LatLngAlt[]): Stage[] {
    const stages: Stage[] = [];
    for (let i = 1; i < coordinates.length; i++) {
      stages.push(new Stage(coordinates[i - 1], coordinates[i]));
    }
    return stages;
  }

  public calculateSnacks(stages: Stage[]): number {
    const stagesReversed = [...stages].reverse();
    return stagesReversed.reduce((snacks: number, stage: Stage) => {
      if (stage.getAltitudeDifference() === 0) {
        return snacks;
      }
      return (stage.getAltitudeDifference() > 0) ?
        snacks + stage.getWalkingDistance() :
        Math.max(snacks - stage.getWalkingDistance(), 0);
    }, 0);
  }

  public findCenter(coordinates: google.maps.LatLng[]): google.maps.LatLng {
    const latSum: number = coordinates
      .map((item: google.maps.LatLng) => item.lat())
      .reduce((a: number, b: number) => a + b);

    const lngSum: number = coordinates
      .map((item: google.maps.LatLng) => item.lng())
      .reduce((a: number, b: number) => a + b);

    return new google.maps.LatLng(
      latSum / coordinates.length,
      lngSum / coordinates.length
    );
  }
}
