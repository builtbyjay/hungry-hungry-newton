import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { IRoute } from '../route';
import { RoutesService } from '../routes.service';

@Injectable()
export class RoutesDetailResolver implements Resolve<Observable<IRoute>> {

  constructor(private routesService: RoutesService) { }

  public resolve(route: ActivatedRouteSnapshot): Observable<IRoute> {
    return this.routesService.find(route.paramMap.get('id'));
  }
}