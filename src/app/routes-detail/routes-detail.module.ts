import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { RoutesDetailComponent } from './routes-detail.component';
import { RoutesDetailResolver } from './routes-detail.resolver';
import { RoutesHelperService } from './routes-helper.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    RoutesDetailComponent
  ],
  providers: [
    RoutesDetailResolver,
    RoutesHelperService
  ]
})
export class RoutesDetailModule { }
