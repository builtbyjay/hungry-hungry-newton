import { Component, OnInit, AfterContentInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRoute, Stage } from '../route';
import { RoutesHelperService } from './routes-helper.service';

@Component({
  selector: 'app-routes-detail',
  templateUrl: './routes-detail.component.html',
  styleUrls: ['./routes-detail.component.scss']
})
export class RoutesDetailComponent implements OnInit, AfterContentInit {
  @ViewChild('gmap') private gmapElement: any;
  public route: IRoute;
  public stages: Stage[];
  public map: google.maps.Map;
  public snacks: number;

  constructor(
    private activatedRoute: ActivatedRoute,
    private routesHelper: RoutesHelperService
  ) { }

  public ngOnInit(): void {
    this.route = this.activatedRoute.snapshot.data.route;
    this.stages = this.routesHelper.createStages(this.route.coordinates);
    this.snacks = Math.floor(this.routesHelper.calculateSnacks(this.stages));
  }

  public ngAfterContentInit(): void {
    this.drawMap();
  }

  private drawMap(): void {
    this.map = new google.maps.Map(
      this.gmapElement.nativeElement,
      {
        center: this.routesHelper.findCenter(this.route.coordinates),
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
    );

    new google.maps.Marker({
      position: this.route.coordinates[0],
      map: this.map,
      label: 'S'
    });

    new google.maps.Marker({
      position: this.route.coordinates[this.route.coordinates.length - 1],
      map: this.map,
      label: `${this.route.coordinates.length - 1}`
    });

    new google.maps.Polyline({
      path: this.route.coordinates,
      strokeColor: '#0000CC',
      strokeOpacity: 0.4,
      map: this.map
    });
  }
}
