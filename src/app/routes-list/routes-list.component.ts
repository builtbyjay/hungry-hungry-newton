import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { IRoute } from '../route';
import { RoutesService } from '../routes.service';

@Component({
  selector: 'app-routes-list',
  templateUrl: './routes-list.component.html'
})
export class RoutesListComponent implements OnInit {
  public routes$: Observable<IRoute[]>;

  constructor(private routesService: RoutesService) { }

  public ngOnInit(): void {
    this.routes$ = this.routesService.all();
  }
}
