export class LatLngAlt extends google.maps.LatLng {
    constructor(
      latitude: number,
      longitude: number,
      private altitude: number
    ) {
     super(latitude, longitude);
    }

    public alt(): number {
      return this.altitude;
    }

    public toJSON(): any {
      return {
        lat: this.lat(),
        lng: this.lng(),
        alt: this.alt()
      };
    }
  }