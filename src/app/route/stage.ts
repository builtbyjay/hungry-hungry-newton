import { LatLngAlt } from './lat-lng-alt';

export class Stage {
  private altitudeDifference: number;
  private horizontalDistance: number;
  private walkingDistance: number;

  constructor(
    public a: LatLngAlt,
    public b: LatLngAlt
  ) {
    this.altitudeDifference = this.b.alt() - this.a.alt();
    this.horizontalDistance = google.maps.geometry.spherical
      .computeLength([ this.a, this.b ]);
    this.walkingDistance = Math.sqrt(
      Math.pow(this.getAltitudeDifference(), 2) +
      Math.pow(this.getHorizontalDistance(), 2)
    );
  }

  public getAltitudeDifference(): number {
    return this.altitudeDifference;
  }

  public getGradient(): number {
    return (this.getAltitudeDifference() / this.getHorizontalDistance()) * 100;
  }

  public getHorizontalDistance(): number {
    return this.horizontalDistance;
  }

  public getWalkingDistance(): number {
    return this.walkingDistance;
  }
}