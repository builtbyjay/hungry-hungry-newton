import { LatLngAlt } from './lat-lng-alt';

export interface ILocationResponse {
  altitude: number;
  longitude: number;
  latitude: number;
}

export interface IRouteResponse {
  id: number;
  name: string;
  locations: ILocationResponse[];
}

export interface IRoute {
  id: number;
  name: string;
  coordinates: LatLngAlt[];
}
